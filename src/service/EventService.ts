import EventRepository from '../repository/EventRepository'
import Event from '../model/Event'

export default class EventService {
  eventRepository: EventRepository;

  constructor() {
    this.eventRepository = new EventRepository()
  }

  async findById(eventId: string): Promise<Event | null> {
    const event = await this.eventRepository.findById(eventId)
    return event
  }

  async findByUUID(eventUUID: string): Promise<Event | null> {
    const event = await this.eventRepository.findByUUID(eventUUID)
    return event
  }
}
