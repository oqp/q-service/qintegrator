require('dotenv').config()

import jwt from 'jsonwebtoken'
import UserRepository from '../repository/UserRepository'
import UserRepositoryInterface from '../repository/interface/IUserRepository'
import User from '../model/User'

export default class UserService {
  userRepository: UserRepositoryInterface;

  constructor() {
    this.userRepository = new UserRepository()
  }

  async refreshToken(user: User): Promise<string> {
    const token = jwt.sign(
      { id: user.id },
      `${process.env.JWT_SECRET}`,
      { expiresIn: 60 * 15 },
    )
    return new Promise<string>((resolve): void => resolve(token))
  }

  async findById(id: string): Promise<User | null> {
    const user: User | null = await this.userRepository.findById(id)
    return user
  }
}
