import AudienceRepository from '../repository/AudienceRepository'
import Audience from '../model/Audience'
import AudienceStatus from '../utility/AudienceStatus'

export default class EventService {
  audienceRepository: AudienceRepository;

  constructor() {
    this.audienceRepository = new AudienceRepository()
  }

  async findByUUID(eventUUID: string, audienceUUID: string): Promise<Audience | null> {
    return await this.audienceRepository.findByUUIDFromDBName(`event-${eventUUID}`, audienceUUID)
  }

  async setAudienceToComplete(eventUUID: string, audience: Audience): Promise<Audience | null> {
    audience.status = AudienceStatus.COMPLETED
    audience.completedAt = new Date()
    const returnAudience = await this.audienceRepository.updateByDBName(`event-${eventUUID}`, audience)
    return returnAudience
  }
}
