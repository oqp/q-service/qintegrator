import Audience from './Audience'

export default class IntegrationAudienceStatus extends Audience {
    queueURL: String;
    sessionTime: Number;
    
    constructor() {
        super()
    }

    async parseFromAudience(audience: Audience): Promise<object> {
        const promiseArray: Promise<object>[] = []
        Object.keys(audience).forEach((key: string) => {
            promiseArray.push(new Promise((resolve) => {
                const updateKey: Record<string, any> = {}
                updateKey[key] = audience[key]
                Object.assign(this, updateKey)
                resolve()
            }))
        })
        return Promise.all(promiseArray)
    }
}