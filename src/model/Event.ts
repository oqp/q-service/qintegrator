import { Schema, SchemaTypes, Document } from 'mongoose'
import { mongoose } from '../config/mongoose'
import BaseModel from './BaseModel'

interface IEvent {
  id: string;
  uuid: string;
  name: string;
  openTime: Date;
  closeTime: Date;
  maxOutflowAmount: number;
  currentQueueNumber: number;
  sessionTime: number;
  redirectURL: string;
  subdomain: string;
  isActive: boolean;
  integrateToken: string;
  waitingRoomId: string;
}

export interface IEventModel extends Document, IEvent {
  id: string;
  uuid: string;
  name: string;
  openTime: Date;
  closeTime: Date;
  maxOutflowAmount: number;
  currentQueueNumber: number;
  sessionTime: number;
  redirectURL: string;
  subdomain: string;
  isActive: boolean;
  integrateToken: string;
  waitingRoomId: string;
}

export const eventSchema = new Schema({
  id: SchemaTypes.ObjectId,
  uuid: { type: String, index: true },
  name: String,
  openTime: Date,
  closeTime: Date,
  maxOutflowAmount: Number,
  currentQueueNumber: Number,
  sessionTime: Number,
  redirectURL: String,
  subdomain: String,
  isActive: Boolean,
  integrateToken: String,
  waitingRoomId: SchemaTypes.ObjectId,
})

export const EventModel = mongoose.model<IEventModel>('Event', eventSchema)

export default class Event extends BaseModel implements IEvent {
  id: string;

  uuid: string = '';

  name: string = '';

  openTime: Date = new Date();

  closeTime: Date = new Date();

  maxOutflowAmount: number = 0;

  currentQueueNumber: number = 0;

  sessionTime: number = 600;

  redirectURL: string = '';

  subdomain: string = '';

  isActive: boolean = false;

  waitingRoomId: string = '';

  integrateToken: string = '';

  toJSON(): object {
    const tempObject = { ...this }
    delete tempObject.integrateToken
    return tempObject
  }
}
