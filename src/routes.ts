import express from 'express'
import { getStatus } from './config/mongoose'
import ServiceStatusResponse from './http/response/ServiceStatusResponse'
import TokenVerification from './middleware/TokenVerification'
import AudienceController from './controller/AudienceController'

const router = express.Router()

router.use(TokenVerification)
router.use('/', AudienceController)

router.get('/status', async (req, res) => {
  const mongodbStatus = getStatus()
  const response: Record<string, ServiceStatusResponse> = {
    mongodb: mongodbStatus,
  }
  let responseStatusCode: number = 200
  Object.keys(response).forEach((key: string) => {
    if (response[key].isOK === false) {
      responseStatusCode = 500
    }
  })
  res.statusCode = responseStatusCode
  res.send(response)
})

export = router
