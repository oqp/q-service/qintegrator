import express from 'express'
import HttpResponse from '../http/response/HTTPResponse'
import EventService from '../service/EventService'
import Hash from '../utility/Hash'

const router = express.Router()

router.use("/event/:event_uuid", async (req, res, next) => {
    const eventService = new EventService()
    const eventUuid = req.params.event_uuid
    const reqIntegrateToken = req.header('x-oqp-integrate-token')
    if (reqIntegrateToken == undefined) {
        res.status(400).json(new HttpResponse(4000, "No Token Provided", null))
    } else {
        const event = await eventService.findByUUID(eventUuid)
        if (event == null) {
            res.status(404).json(new HttpResponse(4004, "Event ID Not found", null))
        } else {
            const isTokenMatch = Hash.isHashMatch(reqIntegrateToken, event.integrateToken)
            if (isTokenMatch) {
                next()
            } else {
                res.status(403).json(new HttpResponse(4003, "Unauthorized", null))
            }
        }
    }
})

export default router