export = class HttpResponse {
  code: number;

  message: string;

  data: object | null;

  constructor(code: number, message: string, data: object | null) {
    this.code = code
    this.message = message
    this.data = data
  }

  toString(): object {
    return {
      code: this.code,
      message: this.message,
      data: this.data,
    }
  }
}
