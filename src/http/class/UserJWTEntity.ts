export default class UserJWTEntity implements Record<string, any> {
  id: string;

  iat: number;

  exp: number;

  static parseJSON(json: string): UserJWTEntity {
    const data: Record<string, any> = JSON.parse(json)
    const newUserJWTEntity = new UserJWTEntity()
    newUserJWTEntity.id = data.id
    newUserJWTEntity.exp = data.exp
    newUserJWTEntity.iat = data.iat
    return newUserJWTEntity
  }
}
