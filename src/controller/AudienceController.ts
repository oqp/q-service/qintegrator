require('dotenv').config()
import express from 'express'
import uuidValidate from 'uuid-validate'
import AudienceService from '../service/AudienceService'
import EventService from '../service/EventService'
import HttpResponse from '../http/response/HTTPResponse'
import IntegrationAudienceStatus from '../model/IntegrationAudienceStatus'
import { logger } from '../config/logger'

const router = express.Router()

router.get('/event/:event_uuid/audience/:audience_uuid',
  async (req, res) => {
    const startTime = new Date().getMilliseconds()
    const eventUUID = req.params.event_uuid
    const audienceUUID = req.params.audience_uuid

    const audienceService = new AudienceService()
    const eventService = new EventService()

    if (uuidValidate(eventUUID, 5) === false) {
      const response = new HttpResponse(4000, 'Invalid Event UUID Format', null)
      res.status(400).json(response)
    } else if (uuidValidate(audienceUUID, 5) === false) {
      const response = new HttpResponse(4000, 'Invalid Audience UUID Format', null)
      res.status(400).json(response)
    } else {
      const audience = await audienceService.findByUUID(eventUUID, audienceUUID)
      const event = await eventService.findByUUID(eventUUID)
      if (event == null) {
        const response = new HttpResponse(4004, 'Event Not Found', null)
        res.status(404).json(response)
      }
      else if (audience == null) {
        const response = new HttpResponse(4004, 'Audience Not Found', null)
        res.status(404).json(response)
      } else {
        const integrationAudienceStatus = new IntegrationAudienceStatus()
        await integrationAudienceStatus.parseFromAudience(audience)
        integrationAudienceStatus.queueURL = `https://${event.subdomain}.${process.env.QCLERK_PRIMARY_HOSTNAME || 'qclerk_primary_hostname_not_set.com'}`
        integrationAudienceStatus.sessionTime = event.sessionTime
        const response = new HttpResponse(200, 'Audience Found', integrationAudienceStatus)
        res.status(200).json(response)
        const endTime = new Date().getMilliseconds()
        logger.info('finished getting audience info', {
          logOrigin: 'audienceController',
          getAudienceInfoDuration: endTime-startTime,
          unit: 'ms',
          eventUUID,
          audienceUUID,
        })
      }
    }
})

router.post('/event/:event_uuid/audience/:audience_uuid/completed',
  async (req, res) => {
    const startTime = new Date().getMilliseconds()
    const eventUUID = req.params.event_uuid
    const audienceUUID = req.params.audience_uuid

    const audienceService = new AudienceService()

    if (uuidValidate(eventUUID, 5) === false) {
      const response = new HttpResponse(4000, 'Invalid Event UUID Format', null)
      res.status(400).json(response)
    } else if (uuidValidate(audienceUUID, 5) === false) {
      const response = new HttpResponse(4000, 'Invalid Audience UUID Format', null)
      res.status(400).json(response)
    } else {
      const audience = await audienceService.findByUUID(eventUUID, audienceUUID)
      if (audience == null) {
        const response = new HttpResponse(4004, 'Audience Not Found', null)
        res.status(404).json(response)
      } else {
        const returnAudience = await audienceService.setAudienceToComplete(eventUUID, audience)
        const response = new HttpResponse(200, 'Audience Updated', returnAudience)
        res.status(200).json(response)
        const endTime = new Date().getMilliseconds()
        logger.info('finished updating audience status to completed', {
          logOrigin: 'audienceController',
          postAudienceCompletedStatusDuration: endTime-startTime,
          unit: 'ms',
          eventUUID,
          audienceUUID,
        })
      }
    }
  })

export default router