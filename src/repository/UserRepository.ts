import UserRepositoryInterface from './interface/IUserRepository'
import User, { UserModel } from '../model/User'

export default class UserRepository implements UserRepositoryInterface {
  async findById(id: string): Promise<User | null> {
    const mongooseUser = await UserModel.findOne({ _id: id })
    if (mongooseUser === null) {
      return null
    }
    const user: User = new User()
    user.parseFromMongoose(mongooseUser)
    return user
  }

  async findByEmail(email: string): Promise<User | null> {
    const mongooseUser = await UserModel.findOne({ email })
    if (mongooseUser === null) {
      return null
    }
    const user: User = new User()
    user.parseFromMongoose(mongooseUser)
    return user
  }

  async save(user: User): Promise<User> {
    const newMongooseUser = await UserModel.create(user)
    const newUser = new User()
    newUser.parseFromMongoose(newMongooseUser)
    return newUser
  }
}
