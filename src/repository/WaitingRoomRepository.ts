import IWaitingRoomRepository from './interface/IWaitingRoomRepository'
import WaitingRoom, { WaitingRoomModel } from '../model/WaitingRoom'

export default class WaitingRoomRepository implements IWaitingRoomRepository {
  async findById(waitingRoomId: string): Promise<WaitingRoom | null> {
    const waitingRoom = await WaitingRoomModel.findOne({ _id: waitingRoomId })
    if (waitingRoom == null) {
      return null
    }
    const returnWaitingRoom = new WaitingRoom()
    returnWaitingRoom.parseFromMongoose(waitingRoom)
    return returnWaitingRoom
  }

  async save(waitingRoom: WaitingRoom): Promise<WaitingRoom> {
    const mongooseWaitingRoom = await WaitingRoomModel.create(waitingRoom)
    const newWaitingRoom = new WaitingRoom()
    newWaitingRoom.parseFromMongoose(mongooseWaitingRoom)
    return newWaitingRoom
  }
}
