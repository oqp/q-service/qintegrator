import UserWaitingRoom from '../../model/UserWaitingRoom'

export default interface IUserWaitingRoomRepository {
  findByUserId(userId: string): Promise<UserWaitingRoom[]>;
  findByWaitingRoomId(waitingRoomId: string): Promise<UserWaitingRoom[]>;
  save(userWaitingRoom: UserWaitingRoom): Promise<UserWaitingRoom>;
}
