require('dotenv').config()

export import mongoose = require('mongoose')
import { logger } from './logger'
import ServiceStatusResponse from '../http/response/ServiceStatusResponse'

mongoose.set('bufferCommands', false)
mongoose.set('useFindAndModify', false)

const db = mongoose.connection
db.on('connecting', () => {
  logger.info('connecting to mongodb server...')
})
db.on('error', (message) => {
  logger.error({ message })
})

export const getStatus = (): ServiceStatusResponse => {
  const returnStatus: ServiceStatusResponse = new ServiceStatusResponse()
  returnStatus.code = db.readyState
  switch (db.readyState) {
    case 0: {
      returnStatus.message = 'disconnected'
      break
    }
    case 1: {
      returnStatus.message = 'connected'
      returnStatus.isOK = true
      break
    }
    case 2: {
      returnStatus.message = 'connecting'
      break
    }
    case 3: {
      returnStatus.message = 'disconnecting'
      break
    }
    default: returnStatus.message = 'disconnecting'
  }
  return returnStatus
}
