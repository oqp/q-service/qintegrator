require('dotenv').config()

import winston = require('winston')
import morgan = require('morgan')
import { TransformableInfo } from 'logform'

const { createLogger, format, transports } = winston

const MESSAGE: any = Symbol.for('message')

const jsonFormatter = (logEntry: TransformableInfo) : TransformableInfo => {
  const base = { timestamp: new Date() }
  const json = Object.assign(base, logEntry)
  logEntry[MESSAGE] = JSON.stringify(json)
  return logEntry
}

export const logger: winston.Logger = createLogger({
  level: process.env.LOG_LEVEL || 'info',
  format: format(jsonFormatter)(),
  transports: [
    new winston.transports.Console({
      handleExceptions: true,
    }),
  ],
})

export const httpLogger = morgan('{"timestamp":":date[iso]","logOrigin":"express","message":{"remoteAddress":":remote-addr","remoteUser":":remote-user","method":":method","url":":url","httpVersion":":http-version","status":":status","res":":res[content-length]","referrer":":referrer","userAgent":":user-agent","responseTime":":response-time ms"}}')
